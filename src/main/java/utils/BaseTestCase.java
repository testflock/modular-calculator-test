package utils;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.nio.file.Paths;

public class BaseTestCase {

    protected WebDriver getChromeDriver() {
        String filePathString = Paths.get(".").toAbsolutePath().normalize().toString()
                + "/src/main/resources/drivers/chromedriver_mac";
        System.setProperty("webdriver.chrome.driver", filePathString);

        WebDriver driver = new ChromeDriver();

        focusBrowser(driver);

        driver.manage().window().maximize();

        return driver;
    }

    protected WebDriver getFirefoxDriver() {
        String filePathString = Paths.get(".").toAbsolutePath().normalize().toString()
                + "/src/main/resources/drivers/geckodriver_mac";
        System.setProperty("webdriver.gecko.driver", filePathString);
        WebDriver driver = new FirefoxDriver();

        driver.manage().window().maximize();

        return driver;
    }

    private void focusBrowser(WebDriver driver) {
        String currentWindowHandle;
        currentWindowHandle = driver.getWindowHandle();

        ((JavascriptExecutor)driver).executeScript("alert('testFlock.org')");
        driver.switchTo().alert().accept();

        driver.switchTo().window(currentWindowHandle);

    }

}
