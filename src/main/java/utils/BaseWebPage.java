package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class BaseWebPage {

    protected WebDriver driver;

    public BaseWebPage(WebDriver driver) {
        this.driver = driver;
    }

    protected void initializeFields() {
        PageFactory.initElements(driver, this);
    }

    public void wait(int seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
