package view;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import utils.BaseWebPage;

public class Standard extends BaseWebPage {

    @FindBy(id = "selectCalculator")
    WebElement viewSelector;

    public Standard(WebDriver driver) {
        super(driver);
        initializeFields();
    }

    public void testStandardView() {
        checkStandardCalculator();

        testAddition();
        initializeCalculator();
        testSubtraction();
        initializeCalculator();
        testMultiplication();
        initializeCalculator();
        testDivision();
    }

    public void checkStandardCalculator() {
        String selectedView = viewSelector.getAttribute("checked");

        if (selectedView == "true") {
            driver.findElement(By.id("switch")).click();
        }

    }

    public void initializeCalculator() {
        driver.findElement(By.id("reset-buttonSC")).click();        // click on CE
        wait(1);
    }

    public void testAddition() {
        driver.findElement(By.id("number-5SC")).click();            // click on 5
        wait(1);
        driver.findElement(By.id("add-buttonSC")).click();          // click on +
        wait(1);
        driver.findElement(By.id("number-6SC")).click();            // click on 6
        wait(1);
        driver.findElement(By.id("calculate-buttonSC")).click();    // click on =
        wait(1);

        Assert.assertTrue(driver.findElement(By.id("input-output")).getText().toString().equals("11"));
    }

    public void testSubtraction() {
        driver.findElement(By.id("number-5SC")).click();                // click on 5
        wait(1);
        driver.findElement(By.id("multiplication-buttonSC")).click();   // click on *
        wait(1);
        driver.findElement(By.id("number-6SC")).click();                // click on 6
        wait(1);
        driver.findElement(By.id("calculate-buttonSC")).click();        // click on =
        wait(1);

        Assert.assertTrue(driver.findElement(By.id("input-output")).getText().toString().equals("30"));
    }

    public void testMultiplication() {
        driver.findElement(By.id("number-1SC")).click();                //
        driver.findElement(By.id("number-0SC")).click();                // click on 10
        wait(1);
        driver.findElement(By.id("subtract-buttonSC")).click();         // click on -
        wait(1);
        driver.findElement(By.id("number-7SC")).click();                // click on 7
        wait(1);
        driver.findElement(By.id("calculate-buttonSC")).click();        // click on =
        wait(1);

        Assert.assertTrue(driver.findElement(By.id("input-output")).getText().toString().equals("3"));
    }

    public void testDivision() {
        driver.findElement(By.id("number-8SC")).click();                // click on 8
        wait(1);
        driver.findElement(By.id("division-button")).click();           // click on /
        wait(1);
        driver.findElement(By.id("number-2SC")).click();                // click on 2
        wait(1);
        driver.findElement(By.id("calculate-buttonSC")).click();        // click on =
        wait(1);

        Assert.assertTrue(driver.findElement(By.id("input-output")).getText().toString().equals("4"));
    }

}
