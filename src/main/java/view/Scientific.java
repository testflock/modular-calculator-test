package view;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import utils.BaseWebPage;

import java.text.DecimalFormat;

public class Scientific extends BaseWebPage{

    public Scientific(WebDriver driver) {
        super(driver);
        initializeFields();
    }

    public void testScientificView() {
        checkScientificCalculator();

        testSin();
        initializeCalculator();
        testCos();
        initializeCalculator();
        testTan();
    }

    public void checkScientificCalculator() {
        String selectedView = driver.findElement(By.id("selectCalculator")).getAttribute("checked");

        if (selectedView != "true") {
            driver.findElement(By.id("switch")).click();
        }
    }

    public void initializeCalculator() {
        driver.findElement(By.id("reset-button")).click();        // click on CE
        wait(1);
    }

    public void testSin() {
        DecimalFormat numberFormat = new DecimalFormat("#.##########");

        driver.findElement(By.id("number-1")).click();                //
        driver.findElement(By.id("number-0")).click();                // click on 10
        wait(1);
        driver.findElement(By.id("sin-button")).click();              // click on 'sin'
        wait(1);
        String actSin = driver.findElement(By.id("input-outputS")).getText().toString();
        double actDoubleSin = Double.parseDouble(actSin);
        String actualSin = numberFormat.format(actDoubleSin) + "";
        System.out.println("Actual SIN:"+ actualSin);

        double angleInDegree = 10;
        double angleInRadian = Math.toRadians(angleInDegree);
        double sin = Math.sin(angleInRadian);
        String expectedSin = numberFormat.format(sin) + "";
        System.out.println("Expected SIN:"+ expectedSin);

        Assert.assertEquals(actualSin, expectedSin);
    }

    public void testCos() {
        DecimalFormat numberFormat = new DecimalFormat("#.##########");

        driver.findElement(By.id("number-1")).click();                //
        driver.findElement(By.id("number-0")).click();                // click on 10
        wait(1);
        driver.findElement(By.id("cos-button")).click();              // click on 'cos'
        wait(1);
        String actCos = driver.findElement(By.id("input-outputS")).getText().toString();
        double actDoubleCos = Double.parseDouble(actCos);
        String actualCos = numberFormat.format(actDoubleCos) + "";
        System.out.println("Actual COS:"+ actualCos);

        double angleInDegree = 10;
        double angleInRadian = Math.toRadians(angleInDegree);
        double cos = Math.cos(angleInRadian);
        String expectedCos = numberFormat.format(cos) + "";
        System.out.println("Expected COS:"+ expectedCos);

        Assert.assertEquals(actualCos, expectedCos);
    }

    public void testTan() {
        DecimalFormat numberFormat = new DecimalFormat("#.##########");

        driver.findElement(By.id("number-1")).click();                //
        driver.findElement(By.id("number-0")).click();                // click on 10
        wait(1);
        driver.findElement(By.id("tan-button")).click();              // click on 'tan'
        wait(1);
        String actTan = driver.findElement(By.id("input-outputS")).getText().toString();
        double actDoubleTan = Double.parseDouble(actTan);
        String actualTan = numberFormat.format(actDoubleTan) + "";
        System.out.println("Actual SIN:"+ actualTan);

        double angleInDegree = 10;
        double angleInRadian = Math.toRadians(angleInDegree);
        double tan = Math.tan(angleInRadian);
        String expectedTan = numberFormat.format(tan) + "";
        System.out.println("Expected TAN:"+ expectedTan);

        Assert.assertEquals(actualTan, expectedTan);
    }

}
