# modular-calculator-test

Sample Calculator Testing Program implemented with Java, Selenium, TestNG with a Module Based Testing Framework.

demo page: http://www.testflock.org/calculator/

## Getting Started


## Authors

Vasilis Petrou | Automation Test Engineer
petrou82@gmail.com


## Acknowledgments

what is modular testing framework?

this involves the creation of independent scripts that represent the modules of the application under test. These modules in turn are used in a hierarchical fashion to build large test cases. 
